package de.dkfz.w410.pageobject;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Base class for web elements. Contains common functionality to be shared among children.
 */
abstract public class BasePageObject {
    protected WebDriver driver;
    public Duration waitDurationInSeconds = Duration.ofSeconds(20);

    protected final By successToastBy = By.className("toast-success");
    protected final By loadingScreenBy = By.cssSelector("div[class='loadmask-msg']");

    public BasePageObject(WebDriver driver) {
        this.driver = driver;
    }

    protected WebElement waitForElemToBeDisplayed(By elemBy) {
        Wait<WebDriver> wait = new WebDriverWait(driver, waitDurationInSeconds);
        wait.until(d -> d.findElement(elemBy).isDisplayed());
        wait.until(ExpectedConditions.elementToBeClickable(elemBy));
        return driver.findElement(elemBy);
    }

    protected WebElement waitForElemToBePresent(By elemBy) {
        Wait<WebDriver> wait = new WebDriverWait(driver, waitDurationInSeconds);
        wait.until(ExpectedConditions.presenceOfElementLocated(elemBy));
        return driver.findElement(elemBy);
    }
}
