package de.dkfz.w410.pageobject.entities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import de.dkfz.w410.pageobject.entities.view.AnimalView;
import de.dkfz.w410.pageobject.pagecomponents.Container;
import de.dkfz.w410.pageobject.pagecomponents.RequestDietContainer;
import de.dkfz.w410.pageobject.pagecomponents.RequestEuthanizeContainer;

/**
 * Represents a row in the animal worksheet
 */
public class Animal extends TableRow {
    private final AnimalView view;

    private final By requestTaskButtonBy = By.xpath("//span[text()='Request Task']");
    private final By animalTaskButtonBy = By.xpath("//span[text()='animal']");
    private final By euthanizeTaskButtonBy = By.xpath("//span[text()='euthanize']");
    private final By dietTaskButtonBy = By.xpath("//span[text()='diet']");

    public Animal(WebDriver driver, AnimalView view, Integer rowNumber, By tableLocator) {
        super(driver, view, rowNumber, tableLocator);
        this.view = view;
    }

    public Animal(WebDriver driver, AnimalView view, Integer rowNumber) {
        super(driver, view, rowNumber);
        this.view = view;
    }

    public String getAnimalId() {
        return this.getCellContent(view.getAnimalIdLocator());
    }

    public String getCageNumber() {
        return this.getCellContent(view.getCageNumberLocator());
    }

    public String getRowNumber() {
        return this.getCellContent(view.getRowNumberLocator());
    }

    public void requestEuthanize() {
        this.rightClickRowNumber();
        this.clickRequestTask()
            .clickAnimalTask()
            .clickEuthanizeTask()
            .run();
    }

    public void requestDiet() {
        this.rightClickRowNumber();
        this.clickRequestTask()
            .clickAnimalTask()
            .clickDietTask()
            .run();
    }

    private Animal clickRequestTask() {
        waitForElemToBeDisplayed(requestTaskButtonBy).click();
        return this;
    }

    private Animal clickAnimalTask() {
        waitForElemToBeDisplayed(animalTaskButtonBy).click();
        return this;
    }

    private Container clickEuthanizeTask() {
        waitForElemToBeDisplayed(euthanizeTaskButtonBy).click();
        return new RequestEuthanizeContainer(driver);
    }

    private Container clickDietTask() {
        waitForElemToBeDisplayed(dietTaskButtonBy).click();
        return new RequestDietContainer(driver);
    }
}
