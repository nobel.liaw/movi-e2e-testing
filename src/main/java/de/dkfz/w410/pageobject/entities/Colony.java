package de.dkfz.w410.pageobject.entities;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import de.dkfz.w410.pageobject.entities.view.ColonyView;
import de.dkfz.w410.pageobject.pagecomponents.EditColonyCardContainer;

/**
 * Represents a row in the colony worksheet.
 */
public class Colony extends TableRow {
    private final ColonyView view;

    private final By deleteColonyBtnBy = By.xpath("//span[text()='Delete Colony']");
    private final By confirmDeleteColonyBtnBy = By.id("btnDlgRefReportOk");
    private final By copyColonyBtnBy = By.xpath("//span[text()='Copy Colony']");

    public Colony(WebDriver driver, ColonyView view, Integer rowNumber, By tableLocator) {
        super(driver, view, rowNumber, tableLocator);
        this.view = view;
    }

    public Colony(WebDriver driver, ColonyView view, Integer rowNumber) {
        super(driver, view, rowNumber);
        this.view = view;
    }

    public String getColonyNumber() {
        return this.getCellContent(view.getColonyNumberLocator());
    }

    public String getColonyName() {
        return this.getCellContent(view.getColonyNameLocator());
    }

    public String getApprovedBy() {
        return this.getCellContent(view.getApprovedByLocator());
    }

    public String getApprovedDate() {
        return this.getCellContent(view.getApprovedDateLocator());
    }

    public Colony delete() {
        this.rightClickRowNumber();
        return this.clickDeleteColonyBtn()
            .confirmPopup()
            .confirmDeleteColonyFinal();
    }

    public void copyAndApprove() {
        this.rightClickRowNumber();
        this.clickCopyColonyBtn()
            .confirmPopup();
        new EditColonyCardContainer(driver).run();
    }

    private Colony clickDeleteColonyBtn() {
        waitForElemToBeDisplayed(deleteColonyBtnBy).click();
        return this;
    }

    private Colony confirmPopup() {
        Wait<WebDriver> wait = new WebDriverWait(driver, waitDurationInSeconds);
        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        alert.accept();
        // Wait until loading screen disappears if it exists
        new WebDriverWait(driver, waitDurationInSeconds).until(ExpectedConditions.invisibilityOfElementLocated(loadingScreenBy));
        return this;
    }

    private Colony confirmDeleteColonyFinal() {
        waitForElemToBeDisplayed(confirmDeleteColonyBtnBy).click();
        // Wait until loading screen disappears if it exists
        new WebDriverWait(driver, waitDurationInSeconds).until(ExpectedConditions.invisibilityOfElementLocated(loadingScreenBy));
        return this;
    }

    private Colony clickCopyColonyBtn() {
        waitForElemToBeDisplayed(copyColonyBtnBy).click();
        return this;
    }
}
