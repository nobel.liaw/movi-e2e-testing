package de.dkfz.w410.pageobject.entities;

import java.util.Objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.pagefactory.ByChained;

import de.dkfz.w410.pageobject.BasePageObject;
import de.dkfz.w410.pageobject.entities.view.View;

public class TableRow extends BasePageObject {
    protected final Integer rowNumber;
    protected final By tableLocator;
    protected final View view;

    public TableRow(WebDriver driver, View view, Integer rowNumber, By tableLocator) {
        super(driver);
        this.view = view;
        this.rowNumber = rowNumber;
        this.tableLocator = tableLocator;
    }

    public TableRow(WebDriver driver, View view, Integer rowNumber) {
        this(driver, view, rowNumber, By.xpath("*"));
    }

    protected String getCellContent(By columnLocator) {
        return this.getCell(columnLocator)
            .getAttribute("textContent");
    }

    protected WebElement getCell(By columnLocator) {
        columnLocator = new ByChained(
            tableLocator,
            columnLocator
        );
        waitForElemToBePresent(columnLocator);
        return driver.findElements(columnLocator)
            .stream()
            .filter(row -> Objects.nonNull(row.getAttribute("y")))
            .filter(row -> row.getAttribute("y").equals(rowNumber.toString()))
            .findFirst()
            .orElseThrow();
    }

    protected void rightClickRowNumber() {
        By rowNumberLocator = new ByChained(
            tableLocator,
            view.getRowNumberLocator()
        );
        new Actions(driver)
            .contextClick(getCell(rowNumberLocator))
            .perform();
    }
}
