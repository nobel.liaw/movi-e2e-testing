package de.dkfz.w410.pageobject.entities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import de.dkfz.w410.pageobject.entities.view.TaskView;
import de.dkfz.w410.pageobject.pagecomponents.PerformDietContainer;
import de.dkfz.w410.pageobject.pagecomponents.PerformEuthanizeContainer;

/**
 * Represents a row in the task worksheet.
 */
public class Task extends TableRow {
    private final TaskView view;

    private final By performTaskButtonBy = By.xpath("//span[text()='Perform Task...']");

    public Task(WebDriver driver, TaskView view, Integer rowNumber, By tableLocator) {
        super(driver, view, rowNumber, tableLocator);
        this.view = view;
    }

    public Task(WebDriver driver, TaskView view, Integer rowNumber) {
        super(driver, view, rowNumber);
        this.view = view;
    }

    public String getRowNumber() {
        return getCellContent(view.getRowNumberLocator());
    }

    public String getTaskNumber() {
        return getCellContent(view.getTaskNumberLocator());
    }

    public String getCageNumber() {
        return getCellContent(view.getCageNumberLocator());
    }

    public String getCreatedBy() {
        return getCellContent(view.getCreatedByLocator());
    }

    public String getTaskType() {
        return getCellContent(view.getTaskTypeLocator());
    }

    public void perform() {
        this.rightClickRowNumber();
        this.clickPerformTaskButton();

        if (getTaskType().equals("euthanize")) {
            new PerformEuthanizeContainer(driver).run();
        } else if (getTaskType().equals("diet")) {
            new PerformDietContainer(driver).run();
        }
    }

    private void clickPerformTaskButton() {
        waitForElemToBeDisplayed(performTaskButtonBy).click();
    }
}
