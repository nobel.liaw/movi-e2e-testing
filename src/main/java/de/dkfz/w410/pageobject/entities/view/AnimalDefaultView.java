package de.dkfz.w410.pageobject.entities.view;

import java.util.Optional;

import org.openqa.selenium.By;

/**
 * Represents the default view in the animal worksheet in MoVi
 */
public class AnimalDefaultView implements AnimalView {

    @Override
    public By getAnimalIdLocator() {
        return By.cssSelector("th[x='0']");
    }

    @Override
    public By getCageNumberLocator() {
        return By.cssSelector("td[x='5']");
    }

    @Override
    public By getRowNumberLocator() {
        return By.cssSelector("[x='-1']");
    }

    @Override
    public Optional<String> getMosaicViewName() {
        return Optional.empty();
    }
}
