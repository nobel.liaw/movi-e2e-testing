package de.dkfz.w410.pageobject.entities.view;

import java.util.Optional;

import org.openqa.selenium.By;

/**
 * Represents the "Disposition3" view in the animal worksheet in MoVi
 */
public class AnimalDispositionView implements AnimalView {

    @Override
    public By getAnimalIdLocator() {
        return By.cssSelector("td[x='1']");
    }

    @Override
    public By getCageNumberLocator() {
        return By.cssSelector("td[x='4']");
    }
    
    @Override
    public By getRowNumberLocator() {
        return By.cssSelector("[x='-1']");
    }

    @Override
    public Optional<String> getMosaicViewName() {
        return Optional.of("Disposition3");
    }
}
