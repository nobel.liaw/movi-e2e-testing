package de.dkfz.w410.pageobject.entities.view;

import org.openqa.selenium.By;

/**
 * Represents a view in the animals worksheet in MoVi
 */
public interface AnimalView extends View {
    public By getAnimalIdLocator();
    public By getCageNumberLocator();
}
