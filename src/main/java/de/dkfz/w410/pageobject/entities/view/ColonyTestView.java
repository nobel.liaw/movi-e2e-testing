package de.dkfz.w410.pageobject.entities.view;

import java.util.Optional;

import org.openqa.selenium.By;

/**
 * Represents the "test_colony" view in the colony worksheet in MoVi
 */
public class ColonyTestView implements ColonyView {

    @Override
    public By getColonyNumberLocator() {
        return By.cssSelector("th[x='0']");
    }

    @Override
    public By getColonyNameLocator() {
        return By.cssSelector("td[x='1']");
    }

    @Override
    public By getApprovedByLocator() {
        return By.cssSelector("td[x='6']");
    }

    @Override
    public By getApprovedDateLocator() {
        return By.cssSelector("td[x='7']");
    }

    @Override
    public Optional<String> getMosaicViewName() {
        return Optional.of("test_colony");
    }

    @Override
    public By getRowNumberLocator() {
        return By.cssSelector("[x='-1']");
    }
}
