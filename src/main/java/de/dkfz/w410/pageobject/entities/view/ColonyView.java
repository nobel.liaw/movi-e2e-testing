package de.dkfz.w410.pageobject.entities.view;

import org.openqa.selenium.By;

/**
 * Represents a view in the colony worksheet in MoVi
 */
public interface ColonyView extends View {
    public By getColonyNumberLocator();
    public By getColonyNameLocator();
    public By getApprovedByLocator();
    public By getApprovedDateLocator();
}
