package de.dkfz.w410.pageobject.entities.view;

import java.util.Optional;

import org.openqa.selenium.By;

public class TaskDefaultView implements TaskView {

    @Override
    public Optional<String> getMosaicViewName() {
        return Optional.empty();
    }

    @Override
    public By getRowNumberLocator() {
        return By.cssSelector("[x='-1']");
    }

    @Override
    public By getTaskNumberLocator() {
        return By.cssSelector("[x='0']");
    }

    @Override
    public By getCageNumberLocator() {
        return By.cssSelector("[x='14']");
    }

    @Override
    public By getCreatedByLocator() {
        return By.cssSelector("[x='22']");
    }

    @Override
    public By getTaskTypeLocator() {
        return By.cssSelector("[x='4']");
    }
    
}
