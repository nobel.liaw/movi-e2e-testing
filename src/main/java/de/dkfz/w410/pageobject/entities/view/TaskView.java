package de.dkfz.w410.pageobject.entities.view;

import org.openqa.selenium.By;

/**
 * Represents a view in the task worksheet in MoVi
 */
public interface TaskView extends View {
    public By getTaskNumberLocator();
    public By getCageNumberLocator();
    public By getCreatedByLocator();
    public By getTaskTypeLocator();
}
