package de.dkfz.w410.pageobject.entities.view;

import java.util.Optional;

import org.openqa.selenium.By;

/**
 * Represents a view in Mosaic Vivarium.
 * Remember to change the code if the view changes, e.g., column added, column deleted, etc.
 */
public interface View {
    public Optional<String> getMosaicViewName();
    public By getRowNumberLocator();
}
