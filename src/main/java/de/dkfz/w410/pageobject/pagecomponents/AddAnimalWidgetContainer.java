package de.dkfz.w410.pageobject.pagecomponents;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import de.dkfz.w410.pageobject.BasePageObject;
import de.dkfz.w410.pageobject.entities.Animal;
import de.dkfz.w410.pageobject.entities.view.AnimalDefaultView;

/**
 * A container to add a new animal
 */
public class AddAnimalWidgetContainer extends BasePageObject implements Container {
    private final String protocol;
    private final String colonyNumber;
    private Animal addedAnimal;

    private final By nextButtonBy = By.cssSelector("input[class='k-button'][value='Next >']");
    private final By currentWidgetStepBy = By.cssSelector("div[class='wizardWidget-stepContainer mosaicTaskParentElement'][style='']");
    private final By textboxSpinnerUpBy = By.cssSelector("i[class='fa fa-caret-up mosaicClickIcon']");
    private final By textboxSpinnerDownBy = By.cssSelector("i[class='fa fa-caret-down mosaicClickIcon']");
    private final By enterProgenyAreaBy = By.cssSelector("div[class='enterProgenyArea']");
    private final By searchFieldBy = By.cssSelector("input[placeholder='Search...']");
    private final By dateTextFieldBy = By.cssSelector("input[data-role='datepicker']");
    private final By warningSignBy = By.cssSelector("i[class*='messageCollectionWarningItem']");
    private final By acceptWarningBtnBy = By.cssSelector("input[value='accept']");
    private final By saveBtnBy = By.cssSelector("input[value='Save']");
    private final By listElementsBy = new ByChained(
        By.cssSelector("ul[class='select2-results mosaicSelectResults']"), // Drop down list
        By.xpath("./li[@data-bind]") // List elements
    );
    private final By animalTableLocator = new ByChained(
        By.xpath("//span[text()='Animals (default view)']"), // Animals text
        By.xpath("./ancestor::div[@class='mosaicAccordionContainer'][1]"), // Ancestor container
        By.cssSelector("table[id*='tableMain']") // The table
    );

    public AddAnimalWidgetContainer(WebDriver driver, String colonyNumber, String protocol) {
        super(driver);
        this.colonyNumber = colonyNumber;
        this.protocol = protocol;
        waitDurationInSeconds = Duration.ofSeconds(30);
    }

    @Override
    public void run() {
        addedAnimal = this.clickNextButton()
            .addOneLiveFemale()
            .clickColonySpinner()
            .typeToColonyTextField()
            .enterDateTextField()
            .clickNextButton()
            .clickProtocolDropDown()
            .typeToProtocolTextField()
            .clickNextButton()
            .clickNextButton()
            .clickNextButton()
            .clickNextButton()
            .clickNextButton()
            .clickWarningSign()
            .acceptWarning()
            .clickNextButton()
            .clickSaveBtn();
    }

    public Animal getAddedAnimal() {
        return addedAnimal;
    }

    private AddAnimalWidgetContainer clickNextButton() {
        waitForElemToBeDisplayed(nextButtonBy).click();
        return this;
    }

    private AddAnimalWidgetContainer addOneLiveFemale() {
        waitForElemToBeDisplayed(new ByChained(currentWidgetStepBy, textboxSpinnerUpBy)).click();
        return this;
    }

    private AddAnimalWidgetContainer clickColonySpinner() {
        waitForElemToBeDisplayed(new ByChained(currentWidgetStepBy, enterProgenyAreaBy, textboxSpinnerDownBy)).click();
        return this;
    }

    private AddAnimalWidgetContainer typeToColonyTextField() {
        waitForElemToBeDisplayed(searchFieldBy).sendKeys(colonyNumber);
        // Wait until list is filtered. First element is the <show inactive colonies> button. Second element is the typed colony.
        Wait<WebDriver> wait = new WebDriverWait(driver, waitDurationInSeconds);
        wait.until(ExpectedConditions.numberOfElementsToBe(listElementsBy, 2));
        // Click the only colony in the list.
        driver.findElements(listElementsBy).getLast().click();
        return this;
    }

    private AddAnimalWidgetContainer enterDateTextField() {
        WebElement dateTextField = waitForElemToBeDisplayed(new ByChained(currentWidgetStepBy, dateTextFieldBy));
        dateTextField.sendKeys("17-Mai-2024");
        dateTextField.sendKeys(Keys.RETURN);
        return this;
    }

    private AddAnimalWidgetContainer clickProtocolDropDown() {
        waitForElemToBeDisplayed(new ByChained(currentWidgetStepBy, textboxSpinnerDownBy)).click();
        return this;
    }

    private AddAnimalWidgetContainer typeToProtocolTextField() {
        waitForElemToBeDisplayed(searchFieldBy).sendKeys(protocol);
        // Wait until list is filtered.
        Wait<WebDriver> wait = new WebDriverWait(driver, waitDurationInSeconds);
        wait.until(ExpectedConditions.numberOfElementsToBe(listElementsBy, 1));
        // Click the only protocol in the list.
        driver.findElement(listElementsBy).click();
        return this;
    }

    private AddAnimalWidgetContainer clickWarningSign() {
        waitForElemToBeDisplayed(warningSignBy).click();
        return this;
    }

    private AddAnimalWidgetContainer acceptWarning() {
        waitForElemToBeDisplayed(acceptWarningBtnBy).click();
        return this;
    }

    private Animal clickSaveBtn() {
        waitForElemToBeDisplayed(saveBtnBy).click();
        // Wait until loading screen disappears if it exists
        new WebDriverWait(driver, waitDurationInSeconds).until(ExpectedConditions.invisibilityOfElementLocated(loadingScreenBy));
        waitForElemToBePresent(animalTableLocator);
        return new Animal(driver, new AnimalDefaultView(), 0, animalTableLocator);
    }
}
