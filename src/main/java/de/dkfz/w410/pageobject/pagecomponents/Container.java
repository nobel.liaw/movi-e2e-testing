package de.dkfz.w410.pageobject.pagecomponents;

/**
 * A container is a widget/dialog that pops up, usually from the left window.
 * It is used to guide user to complete a process/workflow, e.g., request euthanize, perform euthanize, create colony, etc.
 */
public interface Container {
    public void run();
}
