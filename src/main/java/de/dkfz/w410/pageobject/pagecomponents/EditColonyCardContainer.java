package de.dkfz.w410.pageobject.pagecomponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import de.dkfz.w410.pageobject.BasePageObject;

/**
 * A container to edit a colony.
 */
public class EditColonyCardContainer extends BasePageObject implements Container {
    private final By approvalWorkflowTabBy = By.xpath("//span[text()='Approval Workflow']");
    private final By approveColonyBtnBy = By.cssSelector("input[value='Approved']");
    private final By mosaicCardContainerEditBtnBy = By.cssSelector("input[value='Edit'][class='k-button']");
    private final By saveBtnBy = By.cssSelector("input[value='Save']");

    public EditColonyCardContainer(WebDriver driver) {
        super(driver);
    }
    
    @Override
    public void run() {
        this.switchToApprovalWorkflowTab()
            .clickMosaicCardContainerEditBtn()
            .clickApproveColonyBtn()
            .clickSaveBtn();
    }

    private EditColonyCardContainer switchToApprovalWorkflowTab() {
        waitForElemToBeDisplayed(approvalWorkflowTabBy).click();
        return this;
    }

    private EditColonyCardContainer clickMosaicCardContainerEditBtn() {
        waitForElemToBeDisplayed(mosaicCardContainerEditBtnBy).click();
        return this;
    }

    private EditColonyCardContainer clickApproveColonyBtn() {
        waitForElemToBeDisplayed(approveColonyBtnBy).click();
        return this;
    }

    private void clickSaveBtn() {
        waitForElemToBeDisplayed(saveBtnBy).click();
        // This operation has impact on the DB, so wait until it completes
        waitForElemToBeDisplayed(successToastBy);
        // Wait until loading screen disappears if it exists
        new WebDriverWait(driver, waitDurationInSeconds).until(ExpectedConditions.invisibilityOfElementLocated(loadingScreenBy));
    }
}
