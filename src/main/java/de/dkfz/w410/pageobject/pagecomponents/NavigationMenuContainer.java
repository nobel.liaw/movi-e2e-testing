package de.dkfz.w410.pageobject.pagecomponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import de.dkfz.w410.pageobject.BasePageObject;
import de.dkfz.w410.pageobject.entities.Animal;

/**
 * A container to navigate to other worksheet
 */
public class NavigationMenuContainer extends BasePageObject {
    private final By navigationMenuBy = By.cssSelector("div[class='leftNavigationMenuContainer']");
    private final By animalsMenuItemBy = By.xpath("//span[text()='Animals']");
    private final By addAnimalsMenuItemBy = By.xpath("//span[text()='Add Animals']");

    public NavigationMenuContainer(WebDriver driver) {
        super(driver);
    }

    private NavigationMenuContainer clickAnimalsMenuUtem() {
        waitForElemToBeDisplayed(navigationMenuBy).findElement(animalsMenuItemBy).click();
        return this;
    }

    private NavigationMenuContainer clickAddAnimalsMenuItem() {
        waitForElemToBeDisplayed(navigationMenuBy).findElement(addAnimalsMenuItemBy).click();
        return this;
    }

    public Animal addAnimal(String colonyNumber, String protocol) {
        this.clickAnimalsMenuUtem().clickAddAnimalsMenuItem();
        AddAnimalWidgetContainer addAnimalContainer = new AddAnimalWidgetContainer(driver, colonyNumber, protocol);
        addAnimalContainer.run();
        return addAnimalContainer.getAddedAnimal();
    }
}
