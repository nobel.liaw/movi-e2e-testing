package de.dkfz.w410.pageobject.pagecomponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import de.dkfz.w410.pageobject.BasePageObject;

/**
 * A container to create a new colony
 */
public class NewColonyCardContainer extends BasePageObject implements Container {
    private CharSequence colonyName;

    private final By cardContainerBy = By.cssSelector("div[class='mosaicBaseCardContainerDiv']");
    private final By inputTextboxBy = By.cssSelector("input[class='k-textbox']");
    private final By geneticsTabBy = By.xpath("//span[text()='Genetics']");
    private final By trackMutationsCheckboxBy = By.cssSelector("[id^='colonyCardTrackMutations']");
    private final By securityTabBy = By.xpath("//span[text()='Security']");
    private final By warningSignBy = By.cssSelector("i[class*='messageCollectionWarningItem']");
    private final By warningContainerBy = By.id("qtip-0-content");
    private final By acceptWarningBtnBy = By.cssSelector("input[value='accept']");
    private final By saveBtnBy = By.cssSelector("input[value='Save']");

    public NewColonyCardContainer(WebDriver driver, CharSequence colonyName) {
        super(driver);
        this.colonyName = colonyName;
    }

    @Override
    public void run() {
        this.typeColonyName(colonyName)
            .switchToGeneticsTab()
            .uncheckTrackMutationCheckbox()
            .switchToSecurityTab()
            .clickWarningSign()
            .acceptWarning()
            .clickSaveBtn();
    }

    private NewColonyCardContainer typeColonyName(CharSequence name) {
        waitForElemToBeDisplayed(cardContainerBy)
            .findElement(inputTextboxBy)
            .sendKeys(name);
        return this;
    }

    private NewColonyCardContainer switchToGeneticsTab() {
        waitForElemToBeDisplayed(geneticsTabBy).click();
        return this;
    }

    private NewColonyCardContainer uncheckTrackMutationCheckbox() {
        waitForElemToBeDisplayed(trackMutationsCheckboxBy).click();
        return this;
    }

    private NewColonyCardContainer switchToSecurityTab() {
        waitForElemToBeDisplayed(securityTabBy).click();
        return this;
    }

    private NewColonyCardContainer clickWarningSign() {
        waitForElemToBeDisplayed(warningSignBy).click();
        return this;
    }

    private NewColonyCardContainer acceptWarning() {
        waitForElemToBeDisplayed(warningContainerBy)
            .findElement(acceptWarningBtnBy)
            .click();
        return this;
    }

    private void clickSaveBtn() {
        waitForElemToBeDisplayed(saveBtnBy).click();
        // This operation has impact on the DB, so wait until it completes
        waitForElemToBeDisplayed(successToastBy);
        // Wait until loading screen disappears if it exists
        new WebDriverWait(driver, waitDurationInSeconds).until(ExpectedConditions.invisibilityOfElementLocated(loadingScreenBy));
    }

}
