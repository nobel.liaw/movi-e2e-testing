package de.dkfz.w410.pageobject.pagecomponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import de.dkfz.w410.pageobject.BasePageObject;

/**
 * A container to perform diet on an animal
 */
public class PerformDietContainer extends BasePageObject implements Container {
    private final By performDietButton = By.cssSelector("input[value='Perform Diet']");

    public PerformDietContainer(WebDriver driver) {
        super(driver);
    }

    @Override
    public void run() {
        waitForElemToBeDisplayed(performDietButton).click();
        // Wait until two loading screens disappear
        new WebDriverWait(driver, waitDurationInSeconds).until(ExpectedConditions.invisibilityOfElementLocated(loadingScreenBy));
        new WebDriverWait(driver, waitDurationInSeconds).until(ExpectedConditions.visibilityOfElementLocated(loadingScreenBy));
        new WebDriverWait(driver, waitDurationInSeconds).until(ExpectedConditions.invisibilityOfElementLocated(loadingScreenBy));
    }
}
