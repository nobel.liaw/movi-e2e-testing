package de.dkfz.w410.pageobject.pagecomponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import de.dkfz.w410.pageobject.BasePageObject;

/**
 * A container to request diet on an animal
 */
public class RequestDietContainer extends BasePageObject implements Container {
    private final By requestDietButtonBy = By.cssSelector("[value='Request Diet']");

    public RequestDietContainer(WebDriver driver) {
        super(driver);
    }

    @Override
    public void run() {
        this.clickRequestDietButton();
    }

    private RequestDietContainer clickRequestDietButton() {
        waitForElemToBeDisplayed(requestDietButtonBy).click();
        // Wait until loading screen disappears if it exists
        new WebDriverWait(driver, waitDurationInSeconds).until(ExpectedConditions.invisibilityOfElementLocated(loadingScreenBy));
        return this;
    }
}
