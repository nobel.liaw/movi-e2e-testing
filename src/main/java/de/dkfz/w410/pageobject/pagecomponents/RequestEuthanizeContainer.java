package de.dkfz.w410.pageobject.pagecomponents;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import de.dkfz.w410.pageobject.BasePageObject;

/**
 * A container to request euthanization on an animal
 */
public class RequestEuthanizeContainer extends BasePageObject implements Container {
    private final By thisContainerBy = By.className("taskGroupAccordion");
    private final By expandDropDownListBy = new ByChained(
        thisContainerBy,
        By.className("select2-container")
    );
    private final By listElementsBy = new ByChained(
        By.cssSelector("[class='select2-results mosaicSelectResults']"), // Drop down list
        By.xpath("./li[@data-bind]") // List elements
    );
    private final By dispositionInputFieldBy = By.cssSelector("input[placeholder='Search...']");
    private final By requestEuthanizeButtonBy = By.cssSelector("[value='Request Euthanize']");

    public RequestEuthanizeContainer(WebDriver driver) {
        super(driver);
        waitDurationInSeconds = Duration.ofSeconds(30);
    }

    @Override
    public void run() {
        this.clickDispositionDropDownList()
            .typeToDispositionInputField()
            .clickRequestEuthanizeButton();
    }

    private RequestEuthanizeContainer clickDispositionDropDownList() {
        waitForElemToBeDisplayed(expandDropDownListBy).click();
        return this;
    }

    private RequestEuthanizeContainer typeToDispositionInputField() {
        waitForElemToBeDisplayed(dispositionInputFieldBy).sendKeys("07");
        // Wait until list is filtered.
        // First element is expanding the list. Second element is the typed disposition
        Wait<WebDriver> wait = new WebDriverWait(driver, waitDurationInSeconds);
        wait.until(ExpectedConditions.numberOfElementsToBe(listElementsBy, 2));
        driver.findElement(dispositionInputFieldBy).sendKeys(Keys.RETURN);
        return this;
    }

    private RequestEuthanizeContainer clickRequestEuthanizeButton() {
        waitForElemToBeDisplayed(requestEuthanizeButtonBy).click();
        // Wait until loading screen disappears if it exists
        new WebDriverWait(driver, waitDurationInSeconds).until(ExpectedConditions.invisibilityOfElementLocated(loadingScreenBy));
        return this;
    }
}
