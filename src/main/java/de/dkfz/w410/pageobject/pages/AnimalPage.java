package de.dkfz.w410.pageobject.pages;

import org.openqa.selenium.WebDriver;

import de.dkfz.w410.pageobject.BasePageObject;
import de.dkfz.w410.pageobject.entities.Animal;
import de.dkfz.w410.pageobject.entities.view.AnimalView;

/**
 * Models the animal worksheet
 */
public class AnimalPage extends BasePageObject {
    private Animal topRowAnimal;

    public AnimalPage(WebDriver driver, AnimalView view) {
        super(driver);
        this.topRowAnimal = new Animal(driver, view, 0);
    }

    public Animal getTopRowAnimal() {
        return topRowAnimal;
    }
}
