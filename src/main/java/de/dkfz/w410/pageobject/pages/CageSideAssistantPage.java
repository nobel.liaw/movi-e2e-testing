package de.dkfz.w410.pageobject.pages;

import org.openqa.selenium.WebDriver;

import de.dkfz.w410.pageobject.BasePageObject;
import de.dkfz.w410.pageobject.pagecomponents.NavigationMenuContainer;

/**
 * Models the cage side assistant page
 */
public class CageSideAssistantPage extends BasePageObject {
    private NavigationMenuContainer navigationMenuContainer;

    public CageSideAssistantPage(WebDriver driver) {
        super(driver);
        this.navigationMenuContainer = new NavigationMenuContainer(driver);
    }

    public NavigationMenuContainer getNavigationMenu() {
        return navigationMenuContainer;
    }
}
