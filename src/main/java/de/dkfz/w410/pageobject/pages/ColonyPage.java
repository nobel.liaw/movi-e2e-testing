package de.dkfz.w410.pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import de.dkfz.w410.pageobject.BasePageObject;
import de.dkfz.w410.pageobject.entities.Colony;
import de.dkfz.w410.pageobject.entities.view.ColonyView;
import de.dkfz.w410.pageobject.pagecomponents.NewColonyCardContainer;

/**
 * Models the colony page/worksheet
 */
public class ColonyPage extends BasePageObject {
    private final Colony topRowColony;

    private final By createColonyBtnBy = By.xpath("//span[text()='Create New Colony']");
    private final By creationStrategyWithoutStrainRadioBtnBy = By.cssSelector("[id$='ColonyCreationStrategyNormal']");
    private final By createColonyWithoutStrainBtnBy = By.cssSelector("input[value='Create Colony Without Strain']");

    public ColonyPage(WebDriver driver, ColonyView view) {
        super(driver);
        this.topRowColony = new Colony(driver, view, 0);

        if (!driver.getTitle().equals("Colonies")) {
            throw new IllegalStateException("This is not a colony page," + " current page is: " + driver.getCurrentUrl());
        }
    }

    public Colony getTopRowColony() {
        return topRowColony;
    }

    public void requestNewColony(CharSequence newColonyName) {
        this.clickCreateColonyBtn()
            .selectCreationStrategyWithoutStrain()
            .confirmCreateColonyWithoutStrain();
        new NewColonyCardContainer(driver, newColonyName).run();
    }

    private ColonyPage clickCreateColonyBtn() {
        waitForElemToBeDisplayed(createColonyBtnBy).click();
        return this;
    }

    private ColonyPage selectCreationStrategyWithoutStrain() {
        waitForElemToBeDisplayed(creationStrategyWithoutStrainRadioBtnBy).click();
        return this;
    }

    private ColonyPage confirmCreateColonyWithoutStrain() {
        waitForElemToBeDisplayed(createColonyWithoutStrainBtnBy).click();
        return this;
    }
}
