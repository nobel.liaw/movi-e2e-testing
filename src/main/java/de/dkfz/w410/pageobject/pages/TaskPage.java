package de.dkfz.w410.pageobject.pages;

import org.openqa.selenium.WebDriver;

import de.dkfz.w410.pageobject.BasePageObject;
import de.dkfz.w410.pageobject.entities.Task;
import de.dkfz.w410.pageobject.entities.view.TaskView;

/**
 * Models the task worksheet
 */
public class TaskPage extends BasePageObject {
    private Task topRowTask;

    public TaskPage(WebDriver driver, TaskView view) {
        super(driver);
        this.topRowTask = new Task(driver, view, 0);
    }

    public Task getTopRowTask() {
        return topRowTask;
    }
}
