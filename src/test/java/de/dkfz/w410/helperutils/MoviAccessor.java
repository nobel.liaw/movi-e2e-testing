package de.dkfz.w410.helperutils;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * Helper class to access MoVi app. Wraps the WebDriver class. 
 */
public class MoviAccessor {
    private HttpClient client;
    private MoviRequestBuilder requestBuilder = new MoviRequestBuilder();
    private WebDriver driver;

    public MoviAccessor() {
        client = HttpClient.newBuilder()
            .cookieHandler(new CookieManager(null, CookiePolicy.ACCEPT_ALL))
            .build();

        // Initialize MoVi cookie
        sendHttpRequest(requestBuilder.constructCookieRequest());

        initializeDriver();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void goTo(String url) {
        driver.get(url);
    }
    
    /**
     * The colony worksheet can be setup with a particular view or search.
     * This can be done by calling the API with the desired grid in the request parameter.
     */
    public void setupTestColonyWorksheet() {
        sendHttpRequest(requestBuilder.constructTestColonyGridRequest());
        driver.get("https://mosaicdev.inet.dkfz-heidelberg.de/MosaicDev/Vivarium/Colony/Colonies.aspx");
    }

    public void setupTestAnimalWorksheet() {
        sendHttpRequest(requestBuilder.constructTestAnimalGridRequest());
        driver.get("https://mosaicdev.inet.dkfz-heidelberg.de/MosaicDev/Vivarium/Animal/Animals2.aspx");
    }


    /**
     * IMPORTANT: When impersonating, cookies are changed. Remember to update the driver's cookie.
     * The impersonated user should have permissions to access the view and filter required for the test.
     */
    public void impersonateHeike() {
        sendHttpRequest(requestBuilder.constructImpersonateRequest("abendrot"));
        setDriverCookie(getCookie());
    }

    public void impersonateATV101() {
        sendHttpRequest(requestBuilder.constructImpersonateRequest("atv1-101"));
        setDriverCookie(getCookie());
    }

    public boolean isImpersonating() {
        HttpResponse<String> response = sendHttpRequest(requestBuilder.constructIsImpersonateRequest());
        return Boolean.parseBoolean(response.body().toString());
    }

    public void unimpersonate() {
        sendHttpRequest(requestBuilder.constructUnimpersonateRequest());
    }


    private HttpCookie getCookie() {
        return ((CookieManager) client.cookieHandler().get()).getCookieStore().getCookies().getFirst();
    }

    private void initializeDriver() {
        // Setup proxy
        Proxy proxy = new Proxy();
        proxy.setHttpProxy("www-int2.inet.dkfz-heidelberg.de:80");
        ChromeOptions options = new ChromeOptions();
        options.setCapability("proxy", proxy);
        driver = new ChromeDriver(options);

        // Setup cookie
        // To add cookie, driver needs to be in the cookie's domain first
        driver.get("https://mosaicdev.inet.dkfz-heidelberg.de/some404page");
        setDriverCookie(getCookie());

        // Some elements won't load if window is not maximized
        driver.manage().window().maximize();
    }

    private void setDriverCookie(HttpCookie cookie) {
        driver.manage().deleteCookieNamed(cookie.getName());
        driver.manage().addCookie(new Cookie(cookie.getName(), cookie.getValue()));
    }

    private HttpResponse<String> sendHttpRequest(HttpRequest request) {
        try {
            HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
            if (response.statusCode() != 200 && response.statusCode() != 204) {
                throw new RuntimeException("ERROR: Status code returned is " + response.statusCode());
            }
            return response;
        } catch (IOException | InterruptedException e) {
            // This should not happen
            throw new RuntimeException(e);
        }
    }
}
