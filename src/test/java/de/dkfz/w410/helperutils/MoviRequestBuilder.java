package de.dkfz.w410.helperutils;

import java.net.URI;
import java.net.http.HttpRequest;

import de.dkfz.w410.pageobject.entities.view.AnimalDispositionView;

import java.net.http.HttpClient;

/**
 * Helper class to build HTTP requests with fixed grid/parameters.
 */
public class MoviRequestBuilder {
    public HttpRequest constructCookieRequest() {
        return HttpRequest.newBuilder()
            .uri(URI.create("https://mosaicdev.inet.dkfz-heidelberg.de/MosaicDev/customer/Public/Authenticate"))
            .headers("Content-Type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString("{\"Username\": \"api-credentials\", \"Password\": \"api-credentials\"}"))
            .build();
    }

    public HttpRequest constructTestColonyGridRequest() {
        return HttpRequest.newBuilder()
            .uri(URI.create("https://mosaicdev.inet.dkfz-heidelberg.de/MosaicDev/customer/Grid/GetNamedGridRecordset"))
            .header("Content-Type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString("""
                {
                    "RecordsetName": "Vci.MosaicVivarium.MetaXml.Colony.Line",
                    "ViewsetName": "Vci.MosaicVivarium.View.Colony.Line",
                    "FiltersetName": "Vci.MosaicVivarium.Filter.Colony.Line",
                    "TransformsetName": "Vci.MosaicVivarium.Transform.Colony.Line",
                    "ViewName": "test_colony",
                    "FilterName": "Test Colony Search",
                    "TransformName": null
                }
            """))
            .build();
    }

    public HttpRequest constructTestAnimalGridRequest() {
        return HttpRequest.newBuilder()
            .uri(URI.create("https://mosaicdev.inet.dkfz-heidelberg.de/MosaicDev/customer/Grid/GetNamedGridRecordset"))
            .header("Content-Type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString("""
                {
                    "RecordsetName": "Vci.MosaicVivarium.MetaXml.Animal.Mouse.Grid",
                    "ViewsetName": "Vci.MosaicVivarium.View.Animal.Mouse.Grid",
                    "FiltersetName": "Vci.MosaicVivarium.Filter.Animal.Mouse.Grid",
                    "TransformsetName": null,
                    "ViewName": "$view",
                    "FilterName": "Test Animals",
                    "TransformName": null
                  }
            """.replace("$view", new AnimalDispositionView().getMosaicViewName().get()))
            )
            .build();
    }

    public HttpRequest constructImpersonateRequest(String username) {
        return HttpRequest.newBuilder()
            .uri(URI.create("https://mosaicdev.inet.dkfz-heidelberg.de/MosaicDev/customer/Authentication/ImpersonateByUsername?persUsername=" + username))
            .headers("Content-Type", "application/json")
            .version(HttpClient.Version.HTTP_1_1) // IMPORTANT: HTTP 1.1 is required because we are sending a POST request without a body
            .POST(HttpRequest.BodyPublishers.noBody())
            .build();
    }

    public HttpRequest constructIsImpersonateRequest() {
        return HttpRequest.newBuilder()
            .uri(URI.create("https://mosaicdev.inet.dkfz-heidelberg.de/MosaicDev/customer/Authentication/IsImpersonating"))
            .headers("Content-Type", "application/json")
            .version(HttpClient.Version.HTTP_1_1)
            .POST(HttpRequest.BodyPublishers.noBody())
            .build();
    }

    public HttpRequest constructUnimpersonateRequest() {
        return HttpRequest.newBuilder()
            .uri(URI.create("https://mosaicdev.inet.dkfz-heidelberg.de/MosaicDev/customer/Authentication/Unimpersonate"))
            .headers("Content-Type", "application/json")
            .version(HttpClient.Version.HTTP_1_1)
            .POST(HttpRequest.BodyPublishers.noBody())
            .build();
    }
}
