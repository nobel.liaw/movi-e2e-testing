package de.dkfz.w410.helperutils;

import java.util.UUID;

/**
 * Because we want to monitor test colonies easily, we use a particular name format for them.
 */
public class TestColonyName implements CharSequence {
    private String name = "test_colony_" + UUID.randomUUID().toString();

    public static TestColonyName generateTestColonyName() {
        TestColonyName name = new TestColonyName();
        return name;
    }

    @Override
    public int length() {
        return name.length();
    }

    @Override
    public char charAt(int index) {
        return name.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return name.subSequence(start, end);
    }

    @Override
    public String toString() {
        return name;
    }
}
