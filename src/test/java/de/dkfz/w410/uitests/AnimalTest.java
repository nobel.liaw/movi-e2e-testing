package de.dkfz.w410.uitests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import de.dkfz.w410.helperutils.MoviAccessor;
import de.dkfz.w410.pageobject.entities.Animal;
import de.dkfz.w410.pageobject.entities.Task;
import de.dkfz.w410.pageobject.entities.view.AnimalDispositionView;
import de.dkfz.w410.pageobject.entities.view.AnimalView;
import de.dkfz.w410.pageobject.entities.view.TaskDefaultView;
import de.dkfz.w410.pageobject.pages.AnimalPage;
import de.dkfz.w410.pageobject.pages.CageSideAssistantPage;
import de.dkfz.w410.pageobject.pages.TaskPage;

public class AnimalTest {
    private MoviAccessor accessor = new MoviAccessor();
    private CageSideAssistantPage cageSideAssistantPage;
    private AnimalPage animalPage;
    private TaskPage taskPage;
    private String colonyNumber = "6565";
    private String protocol = "EP-Z001A02";
    private AnimalView animalView = new AnimalDispositionView();

    @Test
    public void euthanizeTest() {
        // We want to ensure that we have at least one test animal to test. So we create one.
        createTestAnimal();

        // Request euthanize (impersonate Heike)
        accessor.impersonateHeike();
        accessor.setupTestAnimalWorksheet();
        animalPage = new AnimalPage(accessor.getDriver(), animalView);
        animalPage.getTopRowAnimal()
            .requestEuthanize();


        // Perform euthanize (impersonate ATV101)
        accessor.impersonateATV101();
        accessor.goTo("https://mosaicdev.inet.dkfz-heidelberg.de/MosaicDev/vci_web/workflow/Task.aspx");
        taskPage = new TaskPage(accessor.getDriver(), new TaskDefaultView());
        Task task = taskPage.getTopRowTask();

        // Assert that euthanize request from abendrot was created
        assertEquals("Abendroth, Heike", task.getCreatedBy());

        String taskNumber = task.getTaskNumber();
        task.perform();

        // Assert that task is performed by checking that it disappears from the task page
        assertNotEquals(taskNumber, taskPage.getTopRowTask().getTaskNumber());
    }

    @Test
    public void dietTest() {
        // We want to ensure that we have at least one test animal to test. So we create one.
        createTestAnimal();

        // Request diet (impersonate Heike)
        accessor.impersonateHeike();
        accessor.setupTestAnimalWorksheet();
        animalPage = new AnimalPage(accessor.getDriver(), animalView);
        animalPage.getTopRowAnimal()
            .requestDiet();


        // Perform diet (impersonate ATV101)
        accessor.impersonateATV101();
        accessor.goTo("https://mosaicdev.inet.dkfz-heidelberg.de/MosaicDev/vci_web/workflow/Task.aspx");
        taskPage = new TaskPage(accessor.getDriver(), new TaskDefaultView());
        Task task = taskPage.getTopRowTask();

        // Assert that diet request from abendrot was created
        assertEquals("Abendroth, Heike", task.getCreatedBy());

        String taskNumber = task.getTaskNumber();
        task.perform();

        // Assert that task is performed by checking that it disappears from the task page
        assertNotEquals(taskNumber, taskPage.getTopRowTask().getTaskNumber());
    }

    private Animal createTestAnimal() {
        accessor.goTo("https://mosaicdev.inet.dkfz-heidelberg.de/MosaicDev/Vivarium/CageSideAssistant.aspx");
        cageSideAssistantPage = new CageSideAssistantPage(accessor.getDriver());
        Animal animal = cageSideAssistantPage.getNavigationMenu().addAnimal(colonyNumber, protocol);
        assertNotNull(animal.getAnimalId());
        assertNotNull(animal.getCageNumber());
        return animal;
    }
}
