package de.dkfz.w410.uitests;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.jupiter.api.Test;

import de.dkfz.w410.helperutils.MoviAccessor;
import de.dkfz.w410.helperutils.TestColonyName;
import de.dkfz.w410.pageobject.entities.Colony;
import de.dkfz.w410.pageobject.entities.view.ColonyTestView;
import de.dkfz.w410.pageobject.pages.ColonyPage;
import de.dkfz.w410.rules.Retry;

public class ColonyTest {
    private MoviAccessor accessor = new MoviAccessor();

    private ColonyPage colonyPage;

    // @Rule
    // public Retry retryRule = new Retry(2);

    @Test
    public void createColonyAndDeleteTest() {
        // Create new colony
        accessor.setupTestColonyWorksheet();
        colonyPage = new ColonyPage(accessor.getDriver(), new ColonyTestView());
        String colonyNumber = colonyPage.getTopRowColony().getColonyNumber();
        colonyPage.requestNewColony(TestColonyName.generateTestColonyName());

        // Assert that a new colony is created
        assertNotEquals(colonyNumber, colonyPage.getTopRowColony().getColonyNumber());

        // Delete colony
        colonyNumber = colonyPage.getTopRowColony().getColonyNumber();
        colonyPage.getTopRowColony().delete();
        assertNotEquals(colonyNumber, colonyPage.getTopRowColony().getColonyNumber());
    }

    @Test
    public void copyAndApproveColonyTest() {
        accessor.setupTestColonyWorksheet();
        colonyPage = new ColonyPage(accessor.getDriver(), new ColonyTestView());
        String colonyNumber = colonyPage.getTopRowColony().getColonyNumber();
        colonyPage.getTopRowColony().copyAndApprove();

        Colony copyColony = colonyPage.getTopRowColony();
        assertNotEquals(colonyNumber, copyColony.getColonyNumber());
        assertEquals("api-credentials, api-credentials", copyColony.getApprovedBy());
    }

    @Test
    public void impersonateTest() {
        accessor.impersonateHeike();
        accessor.setupTestColonyWorksheet();

        assertTrue(accessor.isImpersonating());
        accessor.unimpersonate();
        assertFalse(accessor.isImpersonating());
    }
}
